@interface UIKeyboardImpl : UIView
@property(assign) id<UITextInput> delegate;
+(id)sharedInstance;
-(UIResponder*)delegateAsResponder;
@end

@interface UIKBTree : NSObject
@property(assign) NSString* displayString;
@property(assign) int interactionType;
@end

@interface UIKeyboardLayoutStar : UIView
@property(assign) UIKBTree* activeKey;
@property(assign) BOOL didLongPress;
-(BOOL)isShiftKeyBeingHeld;
-(void)touchCancelled:(UITouch*)touch;
@end

@interface SSTouchInfo : NSObject {BOOL changedX;}
@property(readonly,nonatomic) UIKBTree* key;
@property(readonly,nonatomic) CGFloat originX,originY;
@end
@implementation SSTouchInfo
@synthesize key,originX,originY;
-(id)initWithKey:(UIKBTree*)_key origin:(CGPoint)origin {
  if((self=[super init])){
    key=[_key retain];
    originX=origin.x;
    originY=origin.y;
  }
  return self;
}
-(BOOL)incrementX:(CGFloat)dX {
  originX+=dX;
  return changedX?NO:(changedX=YES);
}
-(void)dealloc {
  [key release];
  [super dealloc];
}
@end

%hook UIKeyboardLayoutStar
-(void)touchDragged:(UITouch*)touch {
  static int imove=0;
  UIKeyboardImpl* keyboard=[UIKeyboardImpl sharedInstance];
  SSTouchInfo* info=objc_getAssociatedObject(touch,&imove);
  if(info!=(id)kCFBooleanFalse){
    if(self.didLongPress) __cancelSwipe:{
      objc_setAssociatedObject(touch,&imove,
       (id)kCFBooleanFalse,OBJC_ASSOCIATION_ASSIGN);
    }
    else {
      if(!info){
        UIKBTree* key;
        static ptrdiff_t offset=0;
        if(!offset){offset=ivar_getOffset(class_getInstanceVariable([UIKeyboardImpl class],"m_delegateAdoptsTextInput"));}
        if(*(BOOL*)((void*)keyboard+offset) && (key=self.activeKey) && key.interactionType!=4){
          objc_setAssociatedObject(touch,&imove,info=[[SSTouchInfo alloc]
           initWithKey:key origin:[touch previousLocationInView:self]],
           OBJC_ASSOCIATION_RETAIN_NONATOMIC);
          [info release];
        }
        else {goto __cancelSwipe;}
      }
      const int xstep=20;
      CGPoint point=[touch locationInView:self];
      int offsetX=(point.x-info.originX)/xstep;
      id<UITextInput> input=keyboard.delegate;
      if(offsetX){
        UITextRange* selection=input.selectedTextRange;
        UITextPosition* cursor=[input positionFromPosition:
         (imove==1?selection.end:selection.start) offset:offsetX];
        if(!cursor){
          cursor=(offsetX<0)?input.beginningOfDocument:input.endOfDocument;
          if(!cursor){return;}
        }
        if([info incrementX:offsetX*xstep]){[self touchCancelled:touch];}
        UITextPosition* position[2];
        if(self.isShiftKeyBeingHeld){
          if([input comparePosition:cursor toPosition:(imove==1)?
           (position[0]=selection.start):(position[1]=selection.end)]
           ==(imove==1?NSOrderedAscending:NSOrderedDescending)){
            position[imove]=position[1-imove];
            position[imove=1-imove]=cursor;
          }
          else {position[imove]=cursor;}
        }
        else {position[0]=position[1]=cursor;}
        id<UITextInputDelegate> delegate=input.inputDelegate;
        [delegate selectionWillChange:input];
        input.selectedTextRange=[input textRangeFromPosition:position[0] toPosition:position[1]];
        [delegate selectionDidChange:input];
        return;
      }
      else if(point.y-info.originY<-30){
        NSString* kstr=info.key.displayString;
        if(kstr.length==1){
          SEL action=NULL;
          BOOL targetUM=NO;
          switch(toupper([kstr characterAtIndex:0])){
            case 'A':action=@selector(selectAll:);break;
            case 'B':action=@selector(toggleBoldface:);break;
            case 'C':action=@selector(copy:);break;
            case 'I':action=@selector(toggleItalics:);break;
            case 'S':action=@selector(select:);break;
            case 'U':action=@selector(toggleUnderline:);break;
            case 'V':action=@selector(paste:);break;
            case 'X':action=@selector(cut:);break;
            case 'Y':action=@selector(redo);targetUM=YES;break;
            case 'Z':action=@selector(undo);targetUM=YES;break;
          }
          if(action){
            UIResponder* responder=keyboard.delegateAsResponder;
            if(targetUM){
              NSUndoManager* target=responder.undoManager;
              if(target){[target performSelector:action];}
            }
            else {
              id target=[responder targetForAction:action withSender:keyboard];
              if(target){[responder performSelector:action withObject:keyboard];}
            }
          }
        }
        [self touchCancelled:touch];
        goto __cancelSwipe;
      }
    }
  }
  %orig;
}
%end
