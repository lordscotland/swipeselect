ARCHS = armv7 arm64

include theos/makefiles/common.mk

TWEAK_NAME = SwipeSelect
SwipeSelect_FILES = Tweak.x
SwipeSelect_FRAMEWORKS = UIKit

include $(THEOS_MAKE_PATH)/tweak.mk
